import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TEST {

	@Test
	void test() {
		assertEquals(letterGrade.letterGrade(101),'X');
		assertEquals(letterGrade.letterGrade(95),'A');
		assertEquals(letterGrade.letterGrade(85),'B');
		assertEquals(letterGrade.letterGrade(75),'C');
		assertEquals(letterGrade.letterGrade(65),'D');
		assertEquals(letterGrade.letterGrade(55),'F');
		assertEquals(letterGrade.letterGrade(-1),'X');
	}

}
